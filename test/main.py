#!/usr/bin/env python3

import read_data

import png
import LBP
import hog
import os

import numpy as np
import scipy as sp

from time import time
from scipy import ndimage

import sklearn
from sklearn import decomposition
from sklearn import metrics, cross_validation
from sklearn.grid_search  import GridSearchCV
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler, Normalizer

from datetime import datetime

from copy import copy

PATH = {
    False: {
        'train': '../data/ml14fall_train.dat',
        'test': '../data/ml14fall_test1.dat'
    },
    True: {
        'train': '../data/ml14fall_train_test1.dat',
        'train_nn': '../tmp/code/train_val.txt',
        'test': '../data/ml14fall_test2_no_answer.dat',
        'test_nn': '../tmp/code/test_val.txt',
    }
}
BLUR_LEVEL = 7
IMAGE_SIZE = 50

def bounding_box(X):
    print("Bounding box...")
    Xnew = []
    for img in X:
        W, H = img.shape
        a, b, c, d = 0, W, 0, H
        thr = 0.005
        while(a < W-1 and img[a, :].mean() < thr):
            a += 1
        while(b > a+1 and img[b-1, :].mean() < thr):
            b -= 1
        while(c < H-1 and img[:, c].mean() < thr):
            c += 1
        while(d > c+1 and img[:, d-1].mean() < thr):
            d -= 1
        newim = img[a:b, c:d].copy()
        Xnew.append(newim)
    return Xnew


def resize(X, W=20):
    print("resize")
    Xnew = []
    for img1 in X:
        oW, oH = img1.shape
        trans = (oW > oH)
        if trans:
            img1 = img1.T
        nH = W
        thrs = 1.5
        if oH / oW > thrs:
            nW = int(nH / oH * oW * thrs / 2) * 2
        else:
            nW = W

        img2 = sp.misc.imresize(img1*255, (nW, nH), interp='cubic') / 255
        diff = (W - nW) // 2
        if diff > 0:
            boz = np.zeros((diff, W))
            img2 = np.concatenate((boz, img2, boz), axis=0)
        if trans:
            img2 = img2.T
        Xnew.append(img2)
    return Xnew


def blur(X, size=5):
    print("blur")
    Xnew = []
    for img1 in X:
        img2 = ndimage.uniform_filter(img1, size=size)
        cnt = (img2 > 0.01).sum()
        if(cnt > 0):
            mx = img2.sum() / cnt * 3
            img2 /= mx
            img2 = np.minimum(img2, 1)
        Xnew.append(img2)
    return Xnew


def save_img(img, filename, path=os.path.curdir):

    with open(os.path.join(path, filename), 'wb') as f:
        W, H = img.shape
        writer = png.Writer(W, H, greyscale=True, compression=9)

        newimg = img.T.copy()
        writer.write(f, newimg*255)


def add_feature(X, Y):
    newX = [row[5:45, 5:45] for row in X]
    return resize(newX, W=50), Y.copy()
    """
    Xnew = []
    Ynew = []
    iden = []
    for i in range(2):
        for j in range(2):
            mat = np.zeros((50, 50))
            mat[i*25:(i+1)*25,j*25:(j+1)*25] += 1
            iden.append(mat)
    for t in range(len(X)):
        img = X[t]
        for i in range(4):
            newimg = img * iden[i]
            if newimg.max() < 0.1:
                continue
            if newimg.sum() < 30:
                continue
            Xnew.append(newimg)
            Ynew.append(Y[t])

    for i in range(20):
        save_img(Xnew[i], i, Ynew[i])
    return Xnew, np.array(Ynew)
    """


def flatten(X):
    Xnew = [img.flatten() for img in X]
    return np.array(Xnew)

def intensity_norm(X):
    new_X = []
    for x in X:
        M = max(x.flatten())
        m = min(x.flatten())
        M = max(M,1e-9)
        new_X.append((x-m)/(M-m)*(1-0)+0)
    return new_X

def img_preprocess(X):
    X = intensity_norm(X)
    X = bounding_box(X)
    X = resize(X, IMAGE_SIZE)
    return X


def lbp_feature(train, test, n_pca=100):
    train = LBP.LBP_LIST(train)
    test = LBP.LBP_LIST(test)

    pca = decomposition.RandomizedPCA(n_components=n_pca)
    pca.fit(train)

    train = pca.transform(train)
    test = pca.transform(test)

    return normalize(train, test)


def hog_feature(train, test, n_pca=100):
    args = dict(
        pixels_per_cell=(5, 5),
        orientations=8
    )
    train = hog.HOG_LIST(train, **args)
    test = hog.HOG_LIST(test, **args)

    pca = decomposition.RandomizedPCA(n_components=n_pca)
    pca.fit(train)

    train = pca.transform(train)
    test = pca.transform(test)

    return normalize(train, test)


def blur_feature(train, test, n_pca=100):
    train = blur(train, BLUR_LEVEL)
    test = blur(test, BLUR_LEVEL)

    train = flatten(train)
    test = flatten(test)

    pca = decomposition.RandomizedPCA(n_components=n_pca)
    pca.fit(train)

    train = pca.transform(train)
    test = pca.transform(test)

    return normalize(train, test)


def normalize(train, test):
    scaler = StandardScaler()
    scaler.fit(train)
    train = scaler.transform(train)
    test = scaler.transform(test)

    #norm = Normalizer()
    #train = norm.fit_transform(train)
    #test = norm.fit_transform(test)
    return train, test


def extract_feature(train_img, test_img, nn_data):
    # extract feature
    features = [
        (lbp_feature, dict(n_pca=100)),
        (hog_feature, dict(n_pca=100)),
        (blur_feature, dict(n_pca=50)),
    ]

    trains = []
    tests = []
    for f, args in features:
        train, test = f(train_img, test_img, **args)
        trains.append(train)
        tests.append(test)

    if nn_data != (None, None):
        nn_data = normalize(*nn_data)
        trains.append(nn_data[0])
        tests.append(nn_data[1])

    # merge all feature
    train = np.concatenate(trains, axis=1)
    test = np.concatenate(tests, axis=1)

    return train, test


def scoring(y_true, y_pred, header=''):
    print("%s:" % header)
    print(metrics.classification_report(y_true, y_pred))

    print("%s: track0 score: %f" % (header, track0_score(y_true, y_pred)))
    print("%s: track1 score: %f" % (header, track1_score(y_true, y_pred)))


def track0_score(y_true, y_pred):
    n = len(y_true)
    y_true = y_true - 10 * (y_true > 21)
    y_pred = y_pred - 10 * (y_pred > 21)
    return 1.0 * sum(y_true != y_pred) / n


def track1_score(y_true, y_pred):
    n = len(y_true)
    y_true_class = (y_true > 11)
    y_pred_class = (y_pred > 11)
    y_true_soft = y_true - 10 * (y_true > 21)
    y_pred_soft = y_pred - 10 * (y_pred > 21)
    return 1.0 * (2 * sum(y_true_class != y_pred_class) + sum(y_true != y_pred) + sum(y_true_soft != y_pred_soft)) / n


def run(break_point=4000, run_test=False, submit=False, error_out=False, grid_search=False, use_nn_feature=False, relable=False):
    if use_nn_feature:
        break_point = break_point or 22000
        break_point = min(break_point, 22000)

    # raw data
    X, Y = read_data.read_data(PATH[submit]['train'], break_point, relable=relable)

    if use_nn_feature:
        # will error when submit = False
        X_nn = read_data.read_nn_data(PATH[submit]['train_nn'], break_point)
        X = list(zip(X, X_nn))

    test_size = 0.2 if not run_test else 0.03

    # cross validation
    X_train_raw, X_test_raw, Y_train, Y_test = cross_validation.train_test_split(X, Y, test_size=test_size)

    if use_nn_feature:
        X_train_raw, X_train_nn = zip(*X_train_raw)
        X_test_raw, X_test_nn = zip(*X_test_raw)
    else:
        X_train_nn = None
        X_test_nn = None


    # img preprocessing
    X_train_img = img_preprocess(X_train_raw)
    X_test_img = img_preprocess(X_test_raw)

    X_train, X_test = extract_feature(X_train_img, X_test_img, (X_train_nn, X_test_nn))
    print(X_train.shape)
    print(X_test.shape)

    print("fitting...")

    # search
    if grid_search:
        C_range = 10.0 ** np.arange(-2, 5)
        gamma_range = 10.0 ** np.arange(-5, 3)
        param_grid = dict(gamma=gamma_range, C=C_range)
        clf = GridSearchCV(estimator=SVC(kernel='rbf', class_weight='auto'), param_grid=param_grid, verbose=True, n_jobs=8)
        t0 = time()
        clf.fit(X_train, Y_train)

        print("Best estimator found by grid search:")
        print(clf.best_estimator_)
        print("done in %0.3fs" % (time() - t0))
    else:
        # best
        param_grid = {
            'C': 1.0,
            'gamma': 0.004
        }
        clf = SVC(kernel='rbf', class_weight='auto', **param_grid)
        clf.fit(X_train, Y_train)


    Y_test_pred = clf.predict(X_test) 

    if error_out:
        os.system('rm ./error/*')
        for i, (x, y_true, y_pred) in enumerate(zip(X_test_img, Y_test, Y_test_pred)):
            if y_true != y_pred:
                save_img(x, 'img%05d_t%d-f%d.png' % (i, y_true, y_pred), path='./error/')

    scoring(Y_train, clf.predict(X_train), header='train')
    scoring(Y_test, Y_test_pred, header='valid')

    if run_test:
        X_test_raw, Y_test = read_data.read_data(PATH[submit]['test'], relable=relable)
        if use_nn_feature:
            X_test_nn = read_data.read_nn_data(PATH[submit]['test_nn'])
        else:
            X_test_nn = None
        X_test_img = img_preprocess(X_test_raw)

        _, X_test = extract_feature(X_train_img, X_test_img, (X_train_nn, X_test_nn))

        print("predicting...")
        y_predict = clf.predict(X_test)

        print("predict done")
        time_str = datetime.now().isoformat('_')
        log_file = "./output/predict_%s.out" % time_str
        with open(log_file, 'w') as f:
            for y in y_predict:
                f.write(str(y)+'\n')

        # we have answer !!
        if Y_test[0] != -1:
            scoring(Y_test, y_predict, header='test')


def main():
    run(break_point=4000, run_test=True, submit=False, grid_search=True, use_nn_feature=False, error_out=True, relable=False)

if __name__ == '__main__':
    main()
