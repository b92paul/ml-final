import read_data
import LBP
from sklearn import *
train_data_path = "../data/ml14fall_train.dat"
test_data_path  = "../data/ml14fall_test1_no_answer.dat"

X,Y  = read_data.read_data(train_data_path,10000)
X1,Y1= read_data.read_data(test_data_path ,10000)

LBP_X  = LBP.LBP_LIST(X )
LBP_X1 = LBP.LBP_LIST(X1)
X_train, X_test, Y_train, Y_test = cross_validation.train_test_split(LBP_X, Y, test_size=0.2)

clf = svm.SVC(kernel='poly', gamma=0.01, coef0=0.01, degree=3, verbose=True)
clf.fit(X_train, Y_train)
print ("fitting done")
print ("predicting self")
yh = clf.predict(X_test)
L = len(yh)
err = 0
for i in range(L):
    if(yh[i] != Y_test[i]):
        err += 1
err /= L
print ('Eval =', err)
print (metrics.classification_report(Y_train, clf.predict(X_train)))
print (metrics.classification_report(Y_test, clf.predict(X_test)))

#print LBP_X
