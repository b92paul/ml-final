import png
from sklearn import svm
from sklearn import linear_model
import cv2
import numpy as np
import read_data
def corner(img):
    gray = np.float32(img)
    dst = cv2.cornerHarris(gray,2,3,0.04)
    dst = cv2.dilate(dst,None)
    line = dst>0.5*dst.max()
    corner=[0]*9
    for i in range(len(line)):
        for j in range(len(line[1])):
            if line[i][j]:
                corner[(i/35)*3+(j/41)]+=1
    return corner
def corner_count(filename):
    image = cv2.imread(filename)
    #print image
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray,2,3,0.04)
    dst = cv2.dilate(dst,None)
    line = dst>0.5*dst.max()
    corner=0
    for x in line:
        for y in x:
            if y:
                corner=corner+1
    return corner
def list_of_corner(X):
    res = []
    for x in X:
        res.append(corner(x))
    return np.array(res)

#corner out
f = open('corner_train.out','w')
X,Y = read_data.read_data('../data/ml14fall_train.dat')
tmp = list_of_corner(X)
for x in tmp:
    f.write(str(x)+'\n')
f.close()
f = open('corner_test.out','w')
X,Y = read_data.read_data('../data/ml14fall_test1_no_answer.dat')
tmp = list_of_corner(X)
for x in tmp:
    f.write(str(x)+'\n')
f.close()

'''

data = open('../data/ml14fall_train.dat','r')
[X,Y]=[[],[]]
(ox,oy)=(52,61)
break_point = 20000
print "reading data..."
for t, r in enumerate(data):
    if t==break_point:
        break
    if t%500==0:
        print t
    line1_t = map(lambda x:x.split(':'), r.rstrip('\n').split(' '))
    line1 = line1_t[1:]
    Y.append(int(line1_t[0][0]))
    y = 1
    img1=[0]*12810
    [cx,cy]=[0,0]
    for i in line1:
        o=int(i[0])-1
        (cx,cy)=(cx+o%105,cy+o/105)
        #img1[int(i[0])-1]=float(i[1])
    if len(line1)>0:
        cx=ox-cx/len(line1)
        cy=oy-cy/len(line1)
    for i in line1:
        o=int(i[0])-1
        (a,b)=(o%105+cx,o/105+cy)
        if a>=0 and a<105 and b>=0 and b<122:
            img1[a+b*105]=1
    #write img out to file
    filename = '../image/train_png/img'+'%05d' %t +'_'+str(line1_t[0][0])+'.png'
    #print corner
    img1.append(corner_count(filename))

    X.append(img1)
data.close()
print "data read done"

print "fitting..."
clf = svm.LinearSVC(loss='l1')
print clf.get_params()
clf.fit(X, Y)
print "fitting done"
data = open('../data/ml14fall_test1_no_answer.dat','r')
[X1,Y1]=[[],[]]
(ox,oy)=(52,61)
print "reading test data..."
for t, r in enumerate(data):
    if t==break_point:
        break
    if t%500==0:
        print t
    line1_t = map(lambda x:x.split(':'), r.rstrip('\n').split(' '))
    line1 = line1_t[1:]
    Y1.append(int(line1_t[0][0]))
    y = 1
    img1=[0]*12810
    [cx,cy]=[0,0]
    for i in line1:
        o=int(i[0])-1
        (cx,cy)=(cx+o%105,cy+o/105)
        #img1[int(i[0])-1]=float(i[1])
    if len(line1)>0:
        cx=ox-cx/len(line1)
        cy=oy-cy/len(line1)
    for i in line1:
        o=int(i[0])-1
        (a,b)=(o%105+cx,o/105+cy)
        if a>=0 and a<105 and b>=0 and b<122:
            img1[a+b*105]=255
    #write img out to file    
    filename = '../image/test_png/img'+'%05d' %t +'_'+str(line1_t[0][0])+'.png'
    #print corner
    img1.append(corner_count(filename))

    
    f = open('imgt'+'%05d' %t +'_'+str(line1_t[0][0])+'.png', 'wb')      # binary mode is important
    img_tmp=[]
    for i in range(122):
    	img_tmp.append(img1[105*i:105*(i+1)])
    #print img_tmp
    w = png.Writer(105, 122, greyscale=True,compression=9)
    w.write(f, img_tmp)
    f.close()
    

    X1.append(img1)
data.close()

print "predicting..."
ans= clf.predict(X1)
print "predict done"
f = open("predict_center_trashold.out",'w')
for line in ans:
    f.write(str(line)+'\n')
f.close()
'''
