from skimage.feature import hog
import numpy as np
def HOG_LIST(X, pixels_per_cell=(5, 5), orientations=8):
    print ("HOG processing...")
    res = []
    for t,x in enumerate(X):
        if t%500==0:
            print("HOG process: %d" %t)
        fd= hog(x,
                orientations=orientations,
                pixels_per_cell=pixels_per_cell,
                cells_per_block=(1, 1),
                visualise=False)
        res.append(fd)
    return np.array(res)
