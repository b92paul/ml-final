import numpy as np
import read_data

def thresholed(center, pixles):
    out = []
    for x in pixels:
        if a>=center:
            out.append(1)
        else:
            out.append(0)
    return out
d = 1
D = [(d,0),(d,d),(0,d),(-d,d),(-d,0),(-d,-d),(0,-d),(d,-d)]
Wei = [1,2,4,8,16,32,64,128]
#print zip(D,W)
def get_pixel(img,x,y):
    if x>=0 and x<105 and y>=0 and y<121:
        return img[x][y]
    else:
        return 0
def LBP_unit(img,x,y):
    hs=0
    center = img[x][y]
    for (dx,dy),w in zip(D,W):
        t = center>get_pixel(img,x+dx,y+dy)
        hs = hs+w*t
    return hs
BIN = range(0,257,4)
#print BIN
def LBP_one(img):
    W, H = img.shape
    img_cen = img[d:W-d, d:H-d]
    img_res = np.zeros((W-2*d, H-2*d),dtype=int)
    for (dx,dy),w in zip(D,Wei):
        img_out = img[d+dx:W-d+dx, d+dy:H-d+dy]
        img_dif = img_cen > img_out
        img_res += img_dif * w
    #res = np.histogram(img_res.flatten() , bins=BIN)
    #res = res[0]
    res = np.bincount(img_res.flatten(),minlength=256)
    #print res.shape
    res[0]/=5000
    res[255]/=100
    return res
grid = 5
def LBP(img):
    #img=img>0.07
    W,H = img.shape
    W/=grid
    H/=grid
    res = np.array([])
    for i in range(grid):
        for j in range(grid):
            #print img[i*W:(i+1)*W,j*H:(j+1)*H]
            #res.append(LBP_one(img[i*W:(i+1)*W,j*H:(j+1)*H]))
            res=np.concatenate((res,LBP_one(img[i*W:(i+1)*W,j*H:(j+1)*H])))
    return res
def LBP_LIST(img_list):
    print ("LBP processing...")
    res = []
    for t,img in enumerate(img_list):
        if t%500==0:
            print ("LBP prosess: %d" %t)
        res.append(LBP(img))
    return res
#LBP test
'''
train_data_path="../data/ml14fall_train.dat"
X,Y = read_data.read_data(train_data_path,1)
#print X
tmp = LBP_LIST(X)
print "tmp"
print tmp[0]
#for x in tmp:
#    print x
'''

