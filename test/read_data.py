import numpy as np

def edge_cut(X, cut=5):
    W, H = X.shape
    return X[cut:W-cut, cut:H-cut]


def read_nn_data(path, break_point=None):
    if break_point is None:
        break_point = 1e10

    X = []

    print("reading nn data...")
    with open(path, 'r') as data:
        for i, row in enumerate(data):
            if i == break_point:
                break

            if i % 500 == 0:
                print(i)

            X.append(list(map(float, row.strip().split(" "))))

    print("nn data read done")

    return np.array(X)


def read_data(path, break_point=None, relable=False):
    if break_point is None:
        break_point = 1e10
    X, Y = [], []

    print("reading data...")
    with open(path, 'r') as data:
        for i, row in enumerate(data):
            if i == break_point:
                break

            if i % 500 == 0:
                print(i)

            line = [x.split(':') for x in row.rstrip('\n').split(' ')]

            # Y
            Y.append(int(line[0][0]))

            # X
            line = [(int(i) - 1, float(val)) for i, val in line[1:]]

            img = np.zeros((105, 122))

            for i, val in line:
                a, b = i % 105, i / 105
                img[a, b] = val
            X.append(img)

    Y = np.array(Y)
    if relable:
        Y = Y-(Y>21)*10
    print("data read done")

    return X, Y


def read_corner(path, break_point=100000):
    with open(path, 'r') as data:
        tmp = []

        for row in data:
            row = row[1:len(row)-2].split(' ')

            line = []
            for x in row:
                if x != '':
                    line.append(x)

            tmp.append(line)

    return np.int64(tmp)
