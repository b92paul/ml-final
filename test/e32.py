import png
import math
import numpy as np
import scipy as sp
from scipy import ndimage
from sklearn import svm, linear_model, feature_selection, preprocessing, decomposition
from sklearn import neural_network, pipeline, metrics, cross_validation, grid_search

def read_data(path, break_point = 100000):
    data = open(path,'r')
    [X,Y]=[[],[]]
    (ox,oy)=(52,61)
    print("reading data...")
    for t, r in enumerate(data):
        if t==break_point:
            break
        if t%500==0:
            print(t)
        line1_t = list(map(lambda x:x.split(':'), r.rstrip('\n').split(' ')))
        line1 = line1_t[1:]
        yn = int(line1_t[0][0])
        if yn >= 22:
            yn -= 12
        Y.append(yn)
        y = 1
        img1=np.zeros((105, 122))
        [cx,cy]=[0,0]
        for i in line1:
            o=int(i[0])-1
            (cx,cy)=(cx+o%105,cy+o//105)
            #img1[int(i[0])-1]=float(i[1])
        if len(line1)>0:
            cx=ox-cx//len(line1)
            cy=oy-cy//len(line1)
        for i in line1:
            o=int(i[0])-1
            (a,b)=(o%105+cx,o//105+cy)
            if a>=0 and a<105 and b>=0 and b<122:
                img1[a,b]=float(i[1])

        X.append(img1)
    data.close()
    Y = np.array(Y)
    print("data read done")
    return X, Y

def bbox(X):
    print("Bounding box...")
    Xnew = []
    for img in X:
        W, H = img.shape
        a = 0
        b = W
        c = 0
        d = H
        thr = 0.1
        while(a < W-1 and img[a,:].max()<thr):
            a += 1
        while(b > a+1 and img[b-1,:].max()<thr):
            b -= 1
        while(c < H-1 and img[:,c].max()<thr):
            c += 1
        while(d > c+1 and img[:,d-1].max()<thr):
            d -= 1
        newim = img[a:b,c:d].copy()
        Xnew.append(newim)

    return Xnew

def resize(X, W=20):
    print("resize")
    Xnew = []
    for img1 in X:
        img2 = sp.misc.imresize(img1*255, (W, W), interp='cubic')/255
        Xnew.append(img2)
    return np.array(Xnew)

def blur(X, size=5):
    print("blur")
    Xnew = []
    for img1 in X:
        img2 = ndimage.uniform_filter(img1, size=size)
        cnt = (img2 > 0.01).sum()
        if(cnt > 0):
            mx = img2.sum() / cnt * 3
            img2 /= mx
            img2 = np.minimum(img2, 1)
        Xnew.append(img2)
    return np.array(Xnew)

def save_img(img, num, ans):
    f = open('img'+'%05d' % num +'_'+str(ans)+'.png', 'wb')      # binary mode is important
    W, H = img.shape
    #print img_tmp
    w = png.Writer(W, H, greyscale=True,compression=9)
    w.write(f, img.T*255)
    f.close()

def flatten(X):
    Xnew = []
    for img in X:
        Xnew.append(img.flatten())
    return np.array(Xnew)

X, Y = read_data('../data/ml14fall_train.dat', break_point=1000)
DSZ = len(X)
X = bbox(X)
X = resize(X, 50)
X = blur(X, 7)

for i in range(10):
    save_img(X[i], i, Y[i])
X = flatten(X)

print("preprocessing")
print(X.shape)
pca = decomposition.RandomizedPCA(n_components=50)
pca.fit(X[:500])
X = pca.transform(X)
print(X.shape)
poly = preprocessing.PolynomialFeatures(degree=2, interaction_only=True)
poly.fit(X)
X = poly.transform(X)
print(X.shape)

X_train, X_test, Y_train, Y_test = cross_validation.train_test_split(X, Y, test_size=0.2)

print("fitting...")
# logistic = linear_model.LogisticRegression()
rbm = neural_network.BernoulliRBM(n_iter=20, verbose=True)
# clf = pipeline.Pipeline(steps=[('rbm', rbm), ('logistic', logistic)])
# rbm.fit(X_train)
# X_train = rbm.transform(X_train)
# X_test = rbm.transform(X_test)
# clf = svm.SVC(kernel='poly', gamma=0.1, coef0=0.01, degree=2, verbose=True)
# clf = svm.SVC(verbose=False)
clf = svm.LinearSVC(C=0.001, verbose=False)

param_grid = [
      {'C': [math.pow(2, i) for i in range(-9, -3)]},
      # {'C': [0.01], 'kernel': ['linear']},
      # {'C': [100, 1000, 10000], 'gamma': [1e-1, 1e-2, 1e-3, 1e-4], 'kernel': ['rbf']},
]
gds = grid_search.GridSearchCV(clf, param_grid, n_jobs=-1, cv=3)
# gds.fit(X_train, Y_train)
# cprm = gds.best_params_
# print(cprm)
# sc = gds.sc = gds.score(X_test, Y_test)
# print(sc)

# clf = gds.best_estimator_
print(clf.get_params())
clf.fit(X_train, Y_train)
print("fitting done")

print("predicting self")
# X_test = rbm.transform(X_test)
yh = clf.predict(X_test)
L = len(yh)
err = 0
for i in range(L):
    if(yh[i] != Y_test[i]):
        err += 1
err /= L
print('Eval =', err)
print(metrics.classification_report(Y_train, clf.predict(X_train)))
print(metrics.classification_report(Y_test, clf.predict(X_test)))

# exit()

X1, Y1 = read_data('../data/ml14fall_test1_no_answer.dat')
X1 = bbox(X1)
X1 = resize(X1, 50)
X1 = blur(X1, 7)
X1 = flatten(X1)

print("preprocessing test")
X1 = pca.transform(X1)
X1 = poly.transform(X1)
print("predicting...")
ans= clf.predict(X1)
print("predict done")
f = open("predict_center_test.out",'w')
for line in ans:
    f.write(str(line)+'\n')
f.close()
