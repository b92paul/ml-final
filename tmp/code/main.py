import read_data

# import png
import os

import numpy as np
import scipy as sp

from time import time
from scipy import ndimage

from sklearn import decomposition
from sklearn import metrics, cross_validation
from sklearn.grid_search  import GridSearchCV
from sklearn.svm import SVC

from datetime import datetime

from test_elastic import distortion

TRAIN_PATH = '../../data/ml14fall_train_test1.dat'
TEST_PATH = '../../data/ml14fall_test2_no_answer.dat'
BLUR_LEVEL = 7

def bounding_box(X):
    print("Bounding box...")
    Xnew = []
    for img in X:
        W, H = img.shape
        a, b, c, d = 0, W, 0, H
        thr = 0.005
        while(a < W-1 and img[a, :].mean() < thr):
            a += 1
        while(b > a+1 and img[b-1, :].mean() < thr):
            b -= 1
        while(c < H-1 and img[:, c].mean() < thr):
            c += 1
        while(d > c+1 and img[:, d-1].mean() < thr):
            d -= 1
        newim = img[a:b, c:d].copy()
        Xnew.append(newim)
    return Xnew

def add_sample(X, Y, num=3):
    X_new=[]
    Y_new=[]
    for x, y in zip(X, Y):
        X_new += distortion(x, num)
        Y_new += [y]*num
    X_new = np.array(X_new)
    Y_new = np.array(Y_new)
    return np.concatenate((X, X_new), axis=0), np.concatenate((Y, Y_new))

def resize(X, W=20):
    print("resize")
    Xnew = []
    for img1 in X:
        oW, oH = img1.shape
        trans = (oW > oH)
        if trans:
            img1 = img1.T
        nH = W
        thrs = 1.5
        if oH / oW > thrs:
            nW = int(nH / oH * oW * thrs / 2) * 2
        else:
            nW = W

        img2 = sp.misc.imresize(img1*255, (nW, nH), interp='cubic') / 255
        diff = (W - nW) // 2
        if diff > 0:
            boz = np.zeros((diff, W))
            img2 = np.concatenate((boz, img2, boz), axis=0)
        if trans:
            img2 = img2.T
        Xnew.append(img2)
    return Xnew


def blur(X, size=5):
    print("blur")
    Xnew = []
    for img1 in X:
        img2 = ndimage.uniform_filter(img1, size=size)
        cnt = (img2 > 0.01).sum()
        if(cnt > 0):
            mx = img2.sum() / cnt * 3
            img2 /= mx
            img2 = np.minimum(img2, 1)
        Xnew.append(img2)
    return Xnew


# def save_img(img, num, ans, path=os.path.curdir):
    # filename = 'img%05d_%s.png' % (num, str(ans))

    # with open(os.path.join(path, filename), 'wb') as f:
        # W, H = img.shape
        # writer = png.Writer(W, H, greyscale=True, compression=9)

        # newimg = img.T.copy()
        # writer.write(f, newimg*255)


def add_feature(X, Y):
    newX = [row[5:45, 5:45] for row in X]
    return resize(newX, W=50), Y.copy()
    """
    Xnew = []
    Ynew = []
    iden = []
    for i in range(2):
        for j in range(2):
            mat = np.zeros((50, 50))
            mat[i*25:(i+1)*25,j*25:(j+1)*25] += 1
            iden.append(mat)
    for t in range(len(X)):
        img = X[t]
        for i in range(4):
            newimg = img * iden[i]
            if newimg.max() < 0.1:
                continue
            if newimg.sum() < 30:
                continue
            Xnew.append(newimg)
            Ynew.append(Y[t])

    for i in range(20):
        save_img(Xnew[i], i, Ynew[i])
    return Xnew, np.array(Ynew)
    """


def flatten(X):
    Xnew = [img.flatten() for img in X]
    return np.array(Xnew)


def run(break_point=40000, run_test=False, error_out=False):
    # X, Y = read_data.read_data(TRAIN_PATH, break_point)
    X_train, Y_train = read_data.read_data(TRAIN_PATH, break_point)
    X_test, Y_test = read_data.read_data(TEST_PATH, break_point)

    # test_size = 0.02 if not run_test else 0.05

    # X_train, X_test, Y_train, Y_test = cross_validation.train_test_split(X, Y, test_size=test_size)

    print("preprocessing")

    X_train, Y_train = add_sample(X_train, Y_train, 3)

    X_train = bounding_box(X_train)
    X_test = bounding_box(X_test)

    X_train = resize(X_train, 50)
    X_test = resize(X_test, 50)

    # XtrL = LBP.LBP_LIST(X_train)
    # XteL = LBP.LBP_LIST(X_test)

    # for i in range(30):
        # save_img(X_train[i], Y_train[i], i)

    # X_train = blur(X_train, BLUR_LEVEL)
    # X_test = blur(X_test, BLUR_LEVEL)

    X_train = flatten(X_train)
    X_test = flatten(X_test)

    import gzip, pickle
    f = gzip.open('../../data/beta6.pkl.gz', 'wb')
    pickle.dump((X_train, Y_train), f, 0)
    f.close()
    f = gzip.open('../../data/gamma6.pkl.gz', 'wb')
    pickle.dump((X_test, Y_test), f, 0)
    f.close()
    exit()


    pca = decomposition.RandomizedPCA(n_components=500)
    pca.fit(X_train)
    X_train = pca.transform(X_train)
    X_test = pca.transform(X_test)
    print(X_train.shape)

    pca2 = decomposition.RandomizedPCA(n_components=200)
    pca2.fit(XtrL)
    XtrL = pca2.transform(XtrL)
    XteL = pca2.transform(XteL)

    XtrN = []
    XteN = []
    CONCC = 1/5.0
    print(XtrL[0])
    for i in range(len(X_train)):
        XtrN.append(np.concatenate((X_train[i], XtrL[i]*CONCC)))
    for i in range(len(X_test)):
        XteN.append(np.concatenate((X_test[i], XteL[i]*CONCC)))
    X_train = np.array(XtrN)
    X_test = np.array(XteN)

    print("fitting...")
    # net = neurolab.net.newff([[0.0, 1.0]]*len(X_train[0]), [10,32])
    # net.train(X_train, Y2d, show=1)

    # YY = net.sim(X_test)
    # Y22 = np.zeros(len(X_test))
    # for i in range(len(X_test)):
        # for j in range(32):
            # if YY[i][j] > YY[i][Y22[i]]:
                # Y22[i] = j

    # # print(metrics.classification_report(Y_train, Y11)
    # print(metrics.classification_report(Y_test, Y22))
    # exit()


    # logistic = linear_model.LogisticRegression()
    #rbm = neural_network.BernoulliRBM(verbose=True)
    # clf = pipeline.Pipeline(steps=[('rbm', rbm), ('logistic', logistic)])
    # rbm.fit(X_train, Y_train)
    # X_T = rbm.transform(X_train)
    # save_img(X_T[0], 0, 0)
    # X_train = rbm.transform(X_train)
    # X_test = rbm.transform(X_test)
    #clf = svm.SVC(kernel='rbf', gamma=0.01, coef0=0.01, degree=3, verbose=False)
    # clf = svm.SVC(verbose=False)
    #clf = svm.LinearSVC(C=0.1, verbose=False)
    #0.0001

    param_grid = {
        'C': [3],
        'gamma': [0.005]
    }
    clf = GridSearchCV(SVC(kernel='rbf', class_weight='auto'), param_grid)
    print(clf.get_params())

    t0 = time()
    clf.fit(X_train, Y_train)
    print("Best estimator found by grid search:")
    print(clf.best_estimator_)
    print("done in %0.3fs" % (time() - t0))

    print("predicting self")
    # X_test = rbm.transform(X_test)
    yh = clf.predict(X_test)
    """
    L = len(yh)
    err = 0
    for i in range(L):
        if(yh[i] != Y_test[i]):
            if error_out:
                save_img(Xt_orig[i].reshape((50, 50)), Y_test[i], '%d_%d' % (yh[i], i), path='./error/')
            err += 1
    err /= L
    print('Eval =', err)
    """

    if not run_test:
        print(metrics.classification_report(Y_train, clf.predict(X_train)))
    print(metrics.classification_report(Y_test, clf.predict(X_test)))

    if run_test:
        X1, _ = read_data.read_data(TEST_PATH, break_point)

        X1 = bounding_box(X1)
        X1 = resize(X1, 50)
        XtsL = LBP.LBP_LIST(X1)

        X1 = blur(X1, BLUR_LEVEL)
        X1 = flatten(X1)
        print ("preprocessing test")
        X1 = pca.transform(X1)
        XtsL = pca2.transform(XtsL)
        XtsN = []
        for i in range(len(X1)):
            XtsN.append(np.concatenate((X1[i], XtsL[i]*CONCC)))
        X1 = np.array(XtsN)
        # X1 = poly.transform(X1)
        print("predicting...")
        ans = clf.predict(X1)
        print("predict done")
        f = open("./output/predict_"+str(datetime.now())+".out", 'w')
        for line in ans:
            f.write(str(line)+'\n')
        f.close()


def main():
    run(break_point=22000, run_test=False, error_out=False)

if __name__ == '__main__':
    main()
