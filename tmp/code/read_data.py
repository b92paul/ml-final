import numpy as np

def edge_cut(X, cut=5):
    W, H = X.shape
    return X[cut:W-cut, cut:H-cut]


def read_data(path, break_point=100000):
    X, Y = [], []

    print("reading data...")
    with open(path, 'r') as data:
        for i, row in enumerate(data):
            if i == break_point:
                break

            if i % 500 == 0:
                print(i)

            line = [x.split(':') for x in row.rstrip('\n').split(' ')]

            # Y
            Y.append(int(line[0][0]))

            # X
            line = [(int(i) - 1, float(val)) for i, val in line[1:]]

            img = np.zeros((105, 122))

            for i, val in line:
                a, b = i % 105, i / 105
                img[a, b] = val
            X.append(img)

    Y = np.array(Y)
    print("data read done")

    return X, Y


def read_corner(path, break_point=100000):
    with open(path, 'r') as data:
        tmp = []

        for row in data:
            row = row[1:len(row)-2].split(' ')

            line = []
            for x in row:
                if x != '':
                    line.append(x)

            tmp.append(line)

    return np.int64(tmp)
