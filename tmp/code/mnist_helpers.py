import numpy
import math
from numpy.random import random_integers
from scipy.ndimage import interpolation
from scipy.ndimage.filters import gaussian_filter



def elastic_transform(image, kernel_dim=122, sigma=4, alpha=36, negated=True):
    """
    This method performs elastic transformations on an image by convolving 
    with a gaussian kernel.

    :param image: a numpy nd array
    :kernel_dim: dimension(1-D) of the gaussian kernel
    :param sigma: standard deviation of the kernel
    :param alpha: a multiplicative factor for image after convolution
    :param negated: a flag indicating whether the image is negated or not

    :returns: a nd array transformed image
    """
    # check if the image is a negated one
    image = image*255
    if not negated:
        image = 255-image
    # create an empty image
    result = numpy.zeros(image.shape)
    # print(image.shape)
    # create random displacement fields
    displacement_field_x = numpy.array([[random_integers(-1, 1) for x in range(kernel_dim)] \
                            for y in range(kernel_dim)]) * alpha
    displacement_field_y = numpy.array([[random_integers(-1, 1) for x in range(kernel_dim)] \
                            for y in range(kernel_dim)]) * alpha
    
    # convolve the fields with the gaussian kernel
    displacement_field_x = gaussian_filter(displacement_field_x, sigma)
    displacement_field_y = gaussian_filter(displacement_field_y, sigma)
    #print(displacement_field_x)
    # make the distortrd image by averaging each pixel value to the neighbouring
    # four pixels based on displacement fields
    
    for row in range(image.shape[0]):
        for col in range(image.shape[1]):
            low_ii = row + int(math.floor(displacement_field_y[row, col]))
            high_ii = row + int(math.ceil(displacement_field_y[row, col]))

            low_jj = col + int(math.floor(displacement_field_y[row, col]))
            high_jj = col + int(math.ceil(displacement_field_y[row, col]))

            if low_ii < 0 or low_jj < 0 or high_ii >= image.shape[0] -1 \
               or high_jj >= image.shape[1] - 1:
                continue

            res = image[low_ii, low_jj]/4 + image[low_ii, high_jj]/4 + \
                    image[high_ii, low_jj]/4 + image[high_ii, high_jj]/4

            result[row, col] = res

    return result


