from read_data import read_data
from  mnist_helpers  import *
from skimage.transform import warp, AffineTransform
import os
# import png
from random import uniform as uni
from random import random
#print("OAO")

# def save_img(img, filename, path=os.path.curdir):
    # with open(os.path.join(path, filename), 'wb') as f:
        # W, H = img.shape
        # writer = png.Writer(W, H, greyscale=True, compression=9)

        # newimg = img.T.copy()
        # writer.write(f, newimg*255)
# X, Y = read_data('../data/ml14fall_train_test1.dat',10)

def distortion(X,num=3,saveimage=False):
    res=[]
    for i in range(num):
        #imgtmp = elastic_transform(X, sigma=uni(4,8), alpha=uni(10,30))
        imgtmp = X

        tform = AffineTransform(scale=(uni(0.9,1.1), uni(0.9,1.1)), rotation=(30/180.0*3.14)*(random()-0.5), 
              shear=0.1*(random()-0.5),translation=(uni(-10,10), uni(-10,10) ) )
    
        imgtmp = warp(imgtmp,tform.inverse, output_shape=(105,122))
        res.append(imgtmp)
        #print imgtmp 
        # if saveimage:
            # save_img(imgtmp, "test_ela%d"%i)
    return res
#distortion(X[0],10,True)
